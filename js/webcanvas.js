var canvas, ctx, state;
var storearr = new Array();
var originmousepos;
var step = -1;
var textfont = 'serif';
var button = ['pen','eraser', 'line', 'triangle', 'rectangle', 'circle', 'text'];
var fillbox = document.getElementById('fillbox');
var t_flag = false;
canvas = document.getElementById("mycanvas");
ctx = canvas.getContext('2d');

function getmousepos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}
  
function mousemove(evt) {
    var mousepos = getmousepos(canvas, evt);
    var canvaspic = new Image();
    canvaspic.src = storearr[step];
    if(state == button[0]){//pen
        ctx.lineTo(mousepos.x, mousepos.y);
        ctx.stroke();
    }else if( state == button[1]){//eraser
        ctx.globalCompositeOperation = 'destination-out';
        ctx.lineTo(mousepos.x, mousepos.y);
        ctx.stroke();
    }else if (state == button[2]){//line
        canvaspic.onload = function(){
            ctx.beginPath();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0);
            ctx.moveTo(originmousepos.x, originmousepos.y);
            ctx.lineTo(mousepos.x, mousepos.y);
            ctx.closePath();
            ctx.stroke();
        }  
    }else if (state == button[3]){//triangle
        canvaspic.onload = function(){
            ctx.beginPath();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0);
            ctx.moveTo(originmousepos.x, originmousepos.y);
            ctx.lineTo(mousepos.x, mousepos.y);
            ctx.lineTo(originmousepos.x - (mousepos.x - originmousepos.x), mousepos.y);
            ctx.closePath();
            if(fillbox.checked == true){ctx.fill();}
            else{ctx.stroke();}
        }   
    }
    else if (state == button[4]){//rectangle
        canvaspic.onload = function(){
            ctx.beginPath();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0);
            ctx.moveTo(originmousepos.x, originmousepos.y);
            ctx.lineTo(originmousepos.x, mousepos.y);
            ctx.lineTo(mousepos.x, mousepos.y);
            ctx.lineTo(mousepos.x, originmousepos.y);
            ctx.closePath();
            if(fillbox.checked == true){ctx.fill();}
            else{ctx.stroke();}
        }   
    }
    else if (state == button[5]){//circle
        canvaspic.onload = function(){
            var rd;
            rd = Math.sqrt(Math.pow(Math.abs(mousepos.x - originmousepos.x), 2) + Math.pow(Math.abs(mousepos.y - originmousepos.y), 2))/2;
            ctx.beginPath();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0);
            ctx.arc((originmousepos.x + mousepos.x)/2, (originmousepos.y + mousepos.y)/2, rd, 0, Math.PI*2);
            ctx.closePath();
            if(fillbox.checked == true){ctx.fill();}
            else{ctx.stroke();}
        }   
    }
}
  
canvas.addEventListener('mousedown', function(evt) {
    var mousepos = getmousepos(canvas, evt);
    originmousepos = getmousepos(canvas, evt);
    if(state == 'pen' || state == 'eraser'){
        ctx.beginPath();
    }
    ctx.moveTo(mousepos.x, mousepos.y);
    ctx.lineWidth = document.getElementById('size').value;
    evt.preventDefault();
    if(state != 'text'){
        canvas.addEventListener('mousemove', mousemove, false);
    }
    else if(state == 'text'){
        var textpoint = document.getElementById("text");
        if(t_flag == false){
            textpoint.style = 'display: initial';
            textpoint.style.left = mousepos.x + "px";
            textpoint.style.top = mousepos.y + "px";
            t_flag = true;
            console.log(textpoint.style.left);
            console.log(textpoint.style.top);
        }
        else if(t_flag == true){
            textpoint.style = 'display: none';
            t_flag = false;
        }
    }
    
});


canvas.addEventListener('mouseup', function() {
    if(state == 'pen' || state == 'eraser'){
        ctx.closePath();
    }
    ctx.globalCompositeOperation = "source-over";
    canvas.removeEventListener('mousemove', mousemove, false);
    storepic();
}, false);

function storepic(){
    step++;
    //console.log('step' + step);
    if(step < storearr.length){
        storearr.length = step;
    }
    storearr.push(document.getElementById("mycanvas").toDataURL());
}
function undopic(){
    if(step > 0){
        var canvaspic = new Image();
        step--;
        canvaspic.src = storearr[step];
        canvaspic.onload = function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0);
        }
    //console.log('undo');
    } 
}
function redopic(){
    if(step < storearr.length - 1){
        var canvaspic = new Image();
        step++;
        canvaspic.src = storearr[step];
        canvaspic.onload = function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0);
        }
    //console.log('redo');
    }
}

function readfile(input){
    console.log("a");
    var file = input.files[0];//get input
    console.log(file.type);
    var reader = new FileReader();
    reader.readAsDataURL(file);//turn to base64 type
    reader.onload = function(e){
        drawToCanvas(this.result);
        console.log(this.result.type);
    }
}
function drawToCanvas(imgData){
    var img = new Image();
    img.src = imgData;
    console.log(img.type);
    img.onload = function(){
        ctx.drawImage(img,0,0,300,400);
    }
    storepic();
}

//download
function downloadpic(){
    var download = document.getElementById('download');
    var img = document.getElementById("mycanvas").toDataURL('image/jpeg');
    download.setAttribute("href", img);
}

function changecolor(colorvalue){
    ctx.fillStyle = colorvalue;
    ctx.strokeStyle = colorvalue;
    console.log(colorvalue);
}

function textin(text){
    if(event.keyCode == 13){
        console.log('out');
        print_text(text);
        
    }
}
function print_text(){
    ctx.font = ctx.lineWidth + 'px ' + textfont;
    console.log(ctx.font);
    console.log(ctx.lineWidth);
    ctx.textAlign = "center";
    ctx.fillText(text.value,originmousepos.x, originmousepos.y);
    text.value = "";
}

function changefont(changetext){
    textfont = changetext.value;
}

//store the state
function button_click(i){
    document.getElementById(button[i]).addEventListener('click', function(){
        state = button[i];
        if(state == button[0]){
            canvas.style.cursor = 'wait';
            console.log(canvas.style.cursor);
        }
        if(state ==button[1]){
            canvas.style.cursor = 'zoom-in';
            console.log(canvas.style.cursor);
        }
        if(state ==button[2]){
            canvas.style.cursor = 'not-allowed';
            console.log(canvas.style.cursor);
        }
        if(state ==button[3]){
            canvas.style.cursor = 'not-allowed';
            console.log(canvas.style.cursor);
        }
        else if(state == 'text'){
            canvas.style.cursor = 'text';
            console.log(canvas.style.cursor);
        }
    }, false);
}
for(var i = 0; i < button.length; i++) {
    button_click(i);
}

//undo the graph
document.getElementById("redo").addEventListener('click', function() {
    redopic();
}, false);

//undo the graph
document.getElementById("undo").addEventListener('click', function() {
    undopic();
}, false);
//reset graph
document.getElementById("reset").addEventListener('click', function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        storepic();
    }, false);

window.onload = function(){
    storepic();
    colorpt();
    colorsw();
}