
function colorpt() {
    var xscale, yscale, scalewidth, scaleheight;
    var ctx2 = document.getElementById('colorplate').getContext('2d');
    for (var i = 0; i < 255; i++) {
        for (var j = 0; j < 255; j++) {
            ctx2.fillStyle = 'rgb(' + Math.floor(255 - i) + ',' + Math.floor(255 - j) + ',0)';
            ctx2.fillRect(j , i , 1, 1);
        }
    }
}
//switching color
function colorsw() {
    var xscale, yscale, scalewidth, scaleheight;
    var ctx3 = document.getElementById('colorswitch').getContext('2d');
    for (var j = 0; j < 40; j++) {
        ctx3.fillStyle = 'rgb(' + '255' + ',0,'+ Math.floor((255/40)*j);
        ctx3.fillRect(0 , j ,30,1);
    }
    for (var j = 0; j < 40; j++) {
        ctx3.fillStyle = 'rgb(' + Math.floor(255 - (255/40)*j) + ',0'  + ',255)' ;
        ctx3.fillRect(0 , j + 40 ,30,1);
    }
    for (var j = 0; j < 40; j++) {
        ctx3.fillStyle = 'rgb(' +'0,' + Math.floor((255/40)*j)  + ',255)' ;
        ctx3.fillRect(0 , j + 80 ,30,1);
    }
    for (var j = 0; j < 40; j++) {
        ctx3.fillStyle = 'rgb(' +'0,' + '255,' + Math.floor(255 - (255/40)*j) + ')' ;
        ctx3.fillRect(0 , j + 120 ,30,1);
    }
    for (var j = 0; j < 40; j++) {
        ctx3.fillStyle = 'rgb('+ Math.floor((255/40)*j) + ',255,' + '0)' ;
        ctx3.fillRect(0 , j + 160 ,30,1);
    }
    for (var j = 0; j < 40; j++) {
        ctx3.fillStyle = 'rgb('+ '255,' + Math.floor(255 - (255/40)*j) + ',0)' ;
        ctx3.fillRect(0 , j + 200 ,30,1);
    }
}